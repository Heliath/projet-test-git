### Trombinoscope

À partir d'un template https://startbootstrap.com/ créer un trombinoscope des corsaires : 

- La page contient une entête avec le logo de l'institut
- Une page par promo pour les débutants.
- Une page pour l'ensemble des corsaires avec un filtre pour sélectionner la promo pour les avancés.

Les contraintes : 

- le nom doit s'afficher lors d'un survol de la photo
- les photos doivent être toutes de la même taille (https://www.irfanview.com/)
- La page montre des vignettes (il faut redimensionner les photos)
- Les photos sont cliquables, la page contient des vignettes la photo s'affiche quand on clique dessus (en utilisant lightbox pour les avancés)
- pour les encore plus avancés : lire en PHP le (ou les) dossier des photos pour afficher les photos qu'il contient.
